﻿namespace HZY.Repository.EntityFramework.Annotations;

/// <summary>
/// 表 id
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
public class TableIdAttribute : Attribute
{
    /// <summary>
    /// id 类型
    /// </summary>
    public IdType IdType { get; set; } = IdType.SnowflakeId;

    public TableIdAttribute()
    {

    }

    public TableIdAttribute(IdType idType)
    {
        IdType = idType;
    }

}

/// <summary>
/// id 类型
/// </summary>
public enum IdType
{
    SnowflakeId = 0,
    UUId = 1,
    UUIdString = 2
}
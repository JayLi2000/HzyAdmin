﻿namespace HZY.Core.ApiResultManage;

/// <summary>
/// Api 消息返回类
/// </summary>
public class ApiResult : Dictionary<string, object?>
{
    public const string CODE_KEY = "code";
    public const string MESSAGE_KEY = "message";
    public const string DATA_KEY = "data";

    public ApiResult(int code, string? message, object? data)
    {
        base.Add(CODE_KEY, code);
        base.Add(MESSAGE_KEY, message);
        base.Add(DATA_KEY, data);
    }

    public int Code => (int)base[CODE_KEY]!;
    public string? Message => base[MESSAGE_KEY]?.ToString();
    public object? Data => base[DATA_KEY];


    #region result

    /// <summary>
    /// 返回消息
    /// </summary>
    /// <param name="code"></param>
    /// <param name="message"></param>
    /// <returns></returns>
    public static ApiResult ResultMessage(HttpStatusCode code, string message)
        => new ApiResult((int)code, message, null);

    /// <summary>
    /// 返回数据
    /// </summary>
    /// <param name="code"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public static ApiResult ResultData(HttpStatusCode code, object data)
        => new ApiResult((int)code, null, data);

    /// <summary>
    /// 可返回消息和数据
    /// </summary>
    /// <param name="code"></param>
    /// <param name="message"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public static ApiResult Result(HttpStatusCode code, string message, object data)
        => new ApiResult((int)code, message, data);

    #endregion

    #region result code 可传入 int

    /// <summary>
    /// 返回消息
    /// </summary>
    /// <param name="code"></param>
    /// <param name="message"></param>
    /// <returns></returns>
    public static ApiResult ResultMessage(int code, string message)
        => new ApiResult(code, message, null);

    /// <summary>
    /// 返回数据
    /// </summary>
    /// <param name="code"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public static ApiResult ResultData(int code, object data)
        => new ApiResult(code, null, data);

    /// <summary>
    /// 可返回消息和数据
    /// </summary>
    /// <param name="code"></param>
    /// <param name="message"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public static ApiResult Result(int code, string message, object data)
        => new ApiResult(code, message, data);

    #endregion

    #region Ok

    /// <summary>
    /// 成功 可返回消息
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public static ApiResult OkMessage(string message)
        => ResultMessage(HttpStatusCode.OK, message);

    /// <summary>
    /// 成功 可返回数据
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static ApiResult OkData(object data)
        => ResultData(HttpStatusCode.OK, data);

    /// <summary>
    /// 成功 可返回 消息和数据
    /// </summary>
    /// <param name="message"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public static ApiResult Ok(string message, object data)
        => Result(HttpStatusCode.OK, message, data);

    #endregion

    #region Error

    /// <summary>
    /// Error 可返回消息
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public static ApiResult ErrorMessage(string message)
        => ResultMessage(HttpStatusCode.InternalServerError, message);

    /// <summary>
    /// Error 可返回数据
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static ApiResult ErrorData(object data)
        => ResultData(HttpStatusCode.InternalServerError, data);

    /// <summary>
    /// Error 可返回 消息和数据
    /// </summary>
    /// <param name="message"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public static ApiResult Error(string message, object data)
        => Result(HttpStatusCode.InternalServerError, message, data);

    #endregion


}
